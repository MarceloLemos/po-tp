import argparse
import logging

import numpy as np

import linear_programming


def main(args):
    # Creating logger
    logger = logging.getLogger('simplex')
    logger.setLevel(logging.DEBUG)

    # Creating file handler
    fh = logging.FileHandler('simplex.log', mode='w')
    fh.setLevel(logging.DEBUG)
    formatter = logging.Formatter(
        '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
    )
    fh.setFormatter(formatter)
    logger.addHandler(fh)

    # Set print options in numpy
    np.set_printoptions(linewidth=np.inf)

    logger.info('Reading input file ...')
    lp = linear_programming.read_file(args.file)
    logger.info('Finish reading input file')

    logger.info('Starting solver ...')
    lp.solve()
    logger.info('Finish solver')

    if lp.unlimited:
        print('Status: ilimitado')
    elif lp.infeasible:
        print('Status: inviavel')
    else:
        print('Status: otimo')
        print(f'Objetivo: {round(lp.optimal_value, 5)}')
        print(f'Solucao:\n{lp.solution.round(5)}')


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='Simplex solver for linear programming'
    )
    parser.add_argument(
        '--file', '-f', dest='file', action='store', required=True
    )
    parser.add_argument('--output', '-o', dest='output', action='store')
    args = parser.parse_args()
    main(args)
