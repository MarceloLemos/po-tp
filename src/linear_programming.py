import numpy as np
import logging

logger = logging.getLogger('simplex.linear_programming')

class LP:
    __slots__ = [
        'num_var',
        'var_rest',
        'c',
        'A',
        'b',
        'solution',
        'optimal_value',
        'unlimited',
        'infeasible'
    ]

    def __init__(self, num_var, var_rest, c, A, b, optimal_value=0):
        self.num_var = num_var
        self.var_rest = var_rest
        self.c = c
        self.A = A
        self.b = b
        self.solution = None
        self.optimal_value = optimal_value
        self.unlimited = False
        self.infeasible = False

    def select_pivot(self):
        # Select column
        column = np.argmax(self.c<0)

        # Select row
        min_value = float('inf')
        for index, a in enumerate(self.A[:,column]):
            if a > 0:
                value = self.b[index]/a
                if value < min_value:
                    min_value = value
                    row = index
        return row, column

    def make_pivot(self, row, column):
        # Change pivot value to 1
        self.b[row] /= self.A[row, column]
        self.A[row] /= self.A[row, column]

        # Change all other values in the column to 0
        for i in range(self.A.shape[0]):
            if i != row:
                self.b[i] -= self.b[row] * self.A[i, column]
                self.A[i] -= self.A[row] * self.A[i, column]

        # Update value of the optimal solution
        self.optimal_value -= self.b[row] * self.c[column]

        # Change values of objective funtion
        self.c -= self.A[row] * self.c[column]

    def is_unlimited(self):
        for index, value in enumerate(self.c):
            if value < 0 and np.all(self.A[:, index]<=0):
                return True
        return False

    def is_infeasible(self):
        for constraint, value in zip(self.A, self.b):
            if np.all(constraint>=0) and value < 0:
                return True
        return False

    def make_auxiliar(self):
        # Add identity to A
        eye = np.eye(self.A.shape[0])
        A = np.concatenate((self.A, eye), axis=1)

        # Generate c and update optimal value
        c = np.zeros(A.shape[1])
        for i in range(self.A.shape[1], A.shape[1]):
            c[i] = 1

        optimal = 0
        for constraint, value in zip(A, self.b):
            c -= constraint
            optimal -= value

        return LP(self.num_var, self.var_rest, c, A, self.b, optimal)

    def get_solution(self):
        aux_solution = np.zeros(self.A.shape[1])
        for index, column in enumerate(self.A.T):
                for i in range(self.A.shape[0]):
                    mask = np.zeros(self.A.shape[0])
                    mask[i] = 1
                    if np.all(column==mask):
                        aux_solution[index] = self.b[i]
                        break
        solution = np.zeros(self.num_var)
        num_split = 0
        for i in range(self.num_var):
            index = i + num_split
            if self.var_rest[i] == 1:
                solution[i] = aux_solution[index]
            elif self.var_rest[i] == 0:
                solution[i] = aux_solution[index] - aux_solution[index+1]
        self.solution = solution

    def simplex(self):
        logger.debug('c\n%s', self.c)
        logger.debug('A\n%s', self.A)
        logger.debug('b\n%s', self.b)
        logger.debug('Optimal value: %s', self.optimal_value)
        while(np.any(self.c<0)):
            if self.is_unlimited():
                self.unlimited = True
                logger.debug('LP is unlimited')
                break
            if self.is_infeasible():
                logger.debug('LP is infeasible')
                self.infeasible = True
                break
            row, column = self.select_pivot()
            self.make_pivot(row, column)
            zero_mask = np.absolute(self.A) < 0.0000001
            self.A[zero_mask] = 0.
            zero_mask = np.absolute(self.b) < 0.0000001
            self.b[zero_mask] = 0.
            zero_mask = np.absolute(self.c) < 0.0000001
            self.c[zero_mask] = 0.
            if np.absolute(self.optimal_value) < 0.0000001:
                self.optimal_value = 0.
            logger.debug('c\n%s', self.c)
            logger.debug('A\n%s', self.A)
            logger.debug('b\n%s', self.b)
            logger.debug('Optimal value: %s', self.optimal_value)
        if not self.unlimited and not self.infeasible:
            self.get_solution()

    def solve(self):
        logger.info('Creating auxiliar LP ...')
        auxiliar_lp = self.make_auxiliar()
        logger.info('Finish creating auxiliar LP')
        logger.info('Starting simplex on auxiliar LP')
        auxiliar_lp.simplex()
        logger.info('Finish simplex on auxiliar LP')
        if auxiliar_lp.optimal_value == 0:
            logger.debug('Auxiliar is feasible')

            self.A = auxiliar_lp.A[:, :self.A.shape[1]]
            self.b = auxiliar_lp.b

            # Canonical form
            logger.info('Putting LP in canonical form ...')
            logger.debug('c\n%s', self.c)
            logger.debug('A\n%s', self.A)
            logger.debug('b\n%s', self.b)
            logger.debug('Optimal value: %s', self.optimal_value)
            for i in range(self.A.shape[0]):
                mask = np.zeros(self.A.shape[0])
                mask[i] = 1
                for column_index, column in enumerate(self.A.T):
                    if np.all(column==mask):
                        row_index = np.argmax(column==1)
                        break
                self.make_pivot(row_index, column_index)
                zero_mask = np.absolute(self.A) < 0.00000001
                self.A[zero_mask] = 0.
                zero_mask = np.absolute(self.b) < 0.00000001
                self.b[zero_mask] = 0.
                zero_mask = np.absolute(self.c) < 0.00000001
                self.c[zero_mask] = 0.
                logger.debug('c\n%s', self.c)
                logger.debug('A\n%s', self.A)
                logger.debug('b\n%s', self.b)
                logger.debug('Optimal value: %s', self.optimal_value)
            logger.info('Finish putting LP in canocical form')
            logger.info('Starting simplex on original LP ...')
            self.simplex()
            logger.info('Finish simplex on original LP')
        else:
            logger.debug('Auxiliar is infeasible')
            self.infeasible = True


def read_file(file_name):
    with open(file_name) as file:
        # Read the number of variables
        num_var = int(file.readline())

        # Read the number of constraints
        num_constraints = int(file.readline())

        # Read variables restrictions
        var_rest = np.array([int(x) for x in file.readline().split(' ')])

        # Read objective function
        c = [float(x) for x in file.readline().split(' ')]

        A = []
        b = []
        operators = []

        # Read constraints
        for _ in range(num_constraints):
            constraint = file.readline()
            if '<=' in constraint:
                op = '<='
                variables, a = constraint.split(' <= ')
            elif '>=' in constraint:
                op = '>='
                variables, a = constraint.split(' >= ')
            else:
                op = '=='
                variables, a = constraint.split(' == ')

            variables = variables.split(' ')
            variables = [float(x) for x in variables]

            A.append(variables)
            b.append(int(a))
            operators.append(op)

    A = np.array(A, dtype=np.float)
    b = np.array(b, dtype=np.float)

    # Add slack variables
    for index, op in enumerate(operators):
        if op == '<=':
            slack = np.zeros((num_constraints, 1))
            slack[index] = 1
            A = np.concatenate((A, slack), axis=1)
            c.append(0)
        elif op == '>=':
            slack = np.zeros((num_constraints, 1))
            slack[index] = -1
            A = np.concatenate((A, slack), axis=1)
            c.append(0)

    # Split unrestricted variables
    num_split = 0
    for i in range(num_var):
        if var_rest[i] == 0:
            index = i + num_split
            a_minus = -A[:, index]
            A = np.insert(A, index+1, a_minus, axis=1)
            a_minus = -c[index]
            c = np.insert(c, index+1, a_minus)
            num_split += 1

    # Flip c values
    c = np.negative(c, dtype=np.float)

    # Make b positive
    for i in range(num_constraints):
        if b[i] < 0:
            A[i] = -A[i]
            b[i] = -b[i]

    return LP(num_var, var_rest, c, A, b)
